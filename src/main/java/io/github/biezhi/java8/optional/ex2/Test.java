package io.github.biezhi.java8.optional.ex2;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;


public class Test {

	private void m() {

		// 生成一个空的Optional
		Optional.empty();

		// 不可傳入null,生成一個Optional
		Optional.of(null);

		// 可接受null 會判斷是否為null 且生成一个空的Optional
		Optional.ofNullable(null);

		Optional<String> optName = Optional.ofNullable("www");

		// 判斷是否為null 且給我一個預設值
		optName.orElse("!");

		// 傳入方法 如果optName為null 則回傳方法的回傳值
		optName.orElseGet(() -> "!");

		// 判斷是否為null 回傳布林
		optName.isPresent();

		// ifPresent 如果值存在，就执行使用该值的方法调用，否则什么也不做
		optName.ifPresent((e) -> e.length());

		// 如果有值 會對其處理,並返回處理後的Optional,如果沒值則返回空的Optional
		Optional<Integer> length = optName.map((e) -> e.length());

		// 差別在 方法的回傳 flatMap 回傳的必須也是Optional map則是只要是extends Integer的行
		Optional<Integer> length2 = optName.flatMap((e) -> Optional.of(e.length()));

		// 檢核是否符合條件, 符合則傳回該物 不符合則回傳空的Optional
		optName.filter(null);

		// 取得該Optional的值 如果無值 則拋出NoSuchElementException
		optName.get();

	}

	private static void ifPresent() {
		Optional<Goddess> gn = Optional.ofNullable(new Goddess("波多老師"));
		Optional<NewMan> man = Optional.ofNullable(new NewMan(Optional.ofNullable(null), Optional.of(18)));

		// System.out.println(man.get().getGoddess().isPresent());
		// ;
		// man.orElse(other)
		man.ifPresent(theMan -> filter());
	}

	private static void filter() {
		Optional<Goddess> gn = Optional.ofNullable(new Goddess("波多老師"));
		Optional<NewMan> man = Optional.ofNullable(new NewMan(gn));
		Optional<NewMan> man2 = Optional.ofNullable(null);

		System.out.println(gn.get());
		System.out.println(man.get());
		System.out.println();
		System.out.println();
		System.out.println("man.filter波多 : " + man.filter(m -> m.getGoddess().get().getName().equals("波多老師")));
		System.out.println("man.filter蒼 : " + man.filter(m -> m.getGoddess().get().getName().equals("蒼老師")));
		System.out.println();
		System.out.println("man2.filter : " + man2.filter(m -> m.getGoddess().get().getName().equals("波多老師")));
		System.out.println("man2 : " + man2);
		// 此行會跳錯
		System.out.println("man2.get() : " + man2.get());

	}

	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		Class<?> clz = Class.forName("Test");
		Object o = clz.newInstance();
		Method m = clz.getMethod("filter");
		m.invoke(o, "");
	}

	private void nameNullable() {
		String name = null;
		Optional<String> optName = Optional.ofNullable(name);
		System.out.println(optName.get());
		// get method make NoSuchElementException: No value present
	}

	private void nameIsPresent() {
		String name = null;
		Optional<String> optName = Optional.ofNullable(name);
		if (optName.isPresent()) {
			System.out.println(optName.get());
		} else {
			System.out.println("Name is null.");
		}
	}

	private void nameOrElse() {
		String name = null;
		Optional<String> optName = Optional.ofNullable(name);
		System.out.println(optName.orElse("Name is null."));
	}

	// 使用 OrElseThrow 在無值時丟出例外。
	private void nameOrElseThrow() {
		class MyException extends Exception {
			public MyException(String message) {
				super(message);
			}
		}

		String name = null;
		Optional<String> optName = Optional.ofNullable(name);
		try {
			System.out.println(optName.orElseThrow(() -> new MyException("WHAT! NULL!")));
		} catch (MyException e) {
			System.out.println("MY EXCEPTION! " + e.getMessage());
		}
	}

	public static void test() {
		// Man man = new Man();
		// getGoddessName(man);
		Optional<Goddess> gn = Optional.ofNullable(null);
		Optional<Goddess> gn2 = Optional.ofNullable(new Goddess("波多老師"));
		Optional<NewMan> man2 = Optional.empty();
		Optional<NewMan> man3 = Optional.ofNullable(null);
		Optional<NewMan> man4 = Optional.ofNullable(new NewMan());
		Optional<NewMan> man5 = Optional.ofNullable(new NewMan(gn));
		Optional<NewMan> man6 = Optional.ofNullable(new NewMan(gn2));

		String string = getGoddessName2(man2);
		System.out.println(string);
	}

	public String getGoddessName(Man man) {
		if (man != null) {
			Goddess gn = man.getGoddess();
			if (gn != null) {
				return gn.getName();
			}
		} else {
			Man man2 = new Man();
			man2.setGoddess(new Goddess("蒼老師"));
		}
		return "蒼老師";
	}

	public static String getGoddessName2(Optional<NewMan> man) {
		return man.orElse(new NewMan()).getGoddess().orElse(new Goddess("蒼老師")).getName();
	}
}
