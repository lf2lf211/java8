package io.github.biezhi.java8.optional.ex1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 住址对象
 *
 * @author biezhi
 * @date 2018/2/11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {

	/**
	 * 街道
	 */
	private String street;

	/**
	 * 门牌
	 */
	private String door;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public Address(String street, String door) {
		this.door = door;
		this.street = street;
	}

	public Address() {

	}
}
