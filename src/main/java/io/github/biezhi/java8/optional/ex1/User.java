package io.github.biezhi.java8.optional.ex1;

import java.util.Optional;

import lombok.Data;


/**
 * User
 *
 * @author biezhi
 * @date 2018/2/11
 */
@Data
public class User {

	private String username;
	private String password;
	private Integer age;
	private Address address;

	private Optional<Address> optAddress;

	public User(String username, String password, Integer age, Address address, Optional<Address> optAddress) {
		this.username = username;
		this.password = password;
		this.age = age;
		this.address = address;
		this.optAddress = optAddress;
	}

	public User() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Optional<Address> getOptAddress() {
		return optAddress;
	}

	public void setOptAddress(Optional<Address> optAddress) {
		this.optAddress = optAddress;
	}

}
