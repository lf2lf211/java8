package io.github.biezhi.java8.optional.ex2;

import java.util.Optional;


public class NewMan {

	private Optional<Goddess> goddess = Optional.empty();
	private Optional<Integer> age = Optional.empty();

	public Optional<Integer> getAge() {
		return age;
	}

	public void setAge(Optional<Integer> age) {
		this.age = age;
	}

	public Optional<Goddess> getGoddess() {
		return goddess;
	}

	public void setGoddess(Optional<Goddess> goddess) {
		this.goddess = goddess;
	}

	@Override
	public String toString() {
		return "NewMan [goddess=" + goddess + ", age=" + age + "]";
	}

	public NewMan(Optional<Goddess> goddess, Optional<Integer> age) {
		this.goddess = goddess;
		this.age = age;
	}

	public NewMan(Optional<Goddess> goddess) {
		this.goddess = goddess;
	}

	public NewMan() {

	}
}
