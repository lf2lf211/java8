package io.github.biezhi.java8.optional.ex2;

public class Man {

	private Goddess goddess;

	public Goddess getGoddess() {
		return goddess;
	}

	public void setGoddess(Goddess goddess) {
		this.goddess = goddess;
	}

	public Man(Goddess goddess) {
		this.goddess = goddess;
	}

	public Man() {
	}

	@Override
	public String toString() {
		return "Man [goddess=" + goddess + "]";
	}

}
